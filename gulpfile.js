var del = require('del');
var gulp = require('gulp');
var util = require('gulp-util');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var inject = require('gulp-inject');
var rename = require('gulp-rename');
var series = require('stream-series');
var vendor = require('gulp-concat-vendor');
var runSequence = require('run-sequence');

const APP_FOLDER = '.';
const RESOURCES_FOLDER = APP_FOLDER + '/resources';

const ASSETS_SOURCE_FOLDER = APP_FOLDER + '/assets';

const ASSETS_TARGET_FOLDER = RESOURCES_FOLDER + '/assets';
const VENDOR_TARGET_FOLDER = RESOURCES_FOLDER + '/vendor';

const FONTS_PATTERN = '**/*.{eot,svg,ttf,woff,woff2,otf}';

function copyFonts(source, target) {
    return gulp
        .src(source)
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest(target));
}

gulp.task('clean', function () {
    return del.sync(RESOURCES_FOLDER);
});

gulp.task('vendor:css', function () {

    var sources = [
        'bower_components/**/*.css',
        '!bower_components/**/*theme*.css',
        '!bower_components/**/*.min.css'
    ];

    return gulp
        .src(sources)
        .pipe(concat('vendor.min.css'))
        .pipe(gulp.dest(VENDOR_TARGET_FOLDER + '/css'));

});

gulp.task('vendor:fonts', function () {

    var source = 'bower_components/' + FONTS_PATTERN,
        target = VENDOR_TARGET_FOLDER + '/fonts';

    return copyFonts(source, target);

});

gulp.task('vendor:resources', ['vendor:css', 'vendor:fonts']);

gulp.task('assets:sass', function () {

    var sources = [ASSETS_SOURCE_FOLDER + '/sass/sketch.scss'];

    return gulp
        .src(sources)
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(concat('assets.min.css'))
        .pipe(gulp.dest(ASSETS_TARGET_FOLDER + '/css'));

});

gulp.task('assets:img', function () {

    var sources = [ASSETS_SOURCE_FOLDER + '/img/**/*'];

    return gulp
        .src(sources)
        .pipe(gulp.dest(ASSETS_TARGET_FOLDER + '/img'));

});

gulp.task('assets:fonts', function () {

    var source = ASSETS_SOURCE_FOLDER + '/fonts/' + FONTS_PATTERN,
        target = ASSETS_TARGET_FOLDER + '/fonts';

    return copyFonts(source, target);

});

gulp.task('assets:resources', ['assets:sass', 'assets:img', 'assets:fonts']);

gulp.task('inject:resources', function () {

    var vendorStream = gulp.src([VENDOR_TARGET_FOLDER + '/**/*.css'], {read: false});

    var assetsStream = gulp.src([ASSETS_TARGET_FOLDER + '/**/*.css'], {read: false});

    var opts = {relative: true};

    return gulp
        .src(APP_FOLDER + '/index.html')
        .pipe(inject(series(vendorStream, assetsStream), opts))
        .pipe(gulp.dest(APP_FOLDER));

});

gulp.task('watch', function () {
    gulp.watch(ASSETS_SOURCE_FOLDER + '/sass/**/*.scss', ['assets:sass']);
});

gulp.task('build:dev', function (callback) {
    runSequence('clean', ['assets:resources', 'vendor:resources'], 'inject:resources', callback);
});

gulp.task('default', ['build:dev']);